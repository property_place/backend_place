"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cities_controllers_1 = require("../api/controllers/cities.controllers");
const router = (0, express_1.Router)();
router.get("/getCities/:page/:limit/", cities_controllers_1.getCities);
router.get("/getSearchCities/:valueSearch", cities_controllers_1.getSearchCities);
module.exports = router;
