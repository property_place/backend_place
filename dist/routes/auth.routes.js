"use strict";
/*

    path : 'api/auth'

*/
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_controller_1 = require("../api/controllers/auth.controller");
const { check } = require("express-validator");
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require('../middleware/validate_jwt');
const router = (0, express_1.Router)();
router.post("/signin", [
    check("email", "El email es obligatorio").isEmail(),
    check("password", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
], auth_controller_1.signin);
router.post("/signup", [
    check("name", "El nombre es obligatorio").not().isEmpty(),
    check("email", "El email es obligatorio").isEmail(),
    check("password", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
], auth_controller_1.signup);
router.get("/getUserById/:userId", auth_controller_1.profile);
router.get('/renew', validateJWT, auth_controller_1.renewToken);
module.exports = router;
