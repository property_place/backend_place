"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const categories_controllers_1 = require("../api/controllers/categories.controllers");
const router = (0, express_1.Router)();
router.get("/getCategories", categories_controllers_1.getCategories);
module.exports = router;
