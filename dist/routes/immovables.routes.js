"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const immovables_controllers_1 = require("../api/controllers/immovables.controllers");
const router = (0, express_1.Router)();
router.get("/getImmovable/:immovableId", immovables_controllers_1.getImmovable);
router.post("/getImmovables/:page/:limit/:category_id/:city", immovables_controllers_1.getImmovables);
router.get("/getImmovableOffers/:page/:limit/:category_id/:city", immovables_controllers_1.getImmovableOffers);
module.exports = router;
