"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRolByName = void 0;
const db = require("../../config/database");
const getRolByName = (rolName) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (rolName.length <= 0)
            return null;
        const query = `
    SELECT rol_id FROM roles WHERE  
    to_tsvector(LOWER(rol)) @@  plainto_tsquery(LOWER($1))::tsquery 
  `;
        const role = yield db.query(query, [rolName]);
        const rol = role.rows[0];
        if (!rol) {
            return "ROL002";
        }
        const id = rol['rol_id'];
        return id;
    }
    catch (error) {
        return "ROL002";
    }
});
exports.getRolByName = getRolByName;
