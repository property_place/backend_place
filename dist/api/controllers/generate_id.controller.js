"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cityId = exports.personId = exports.userId = void 0;
const db = require("../../config/database");
const generateId_1 = require("../../middleware/generateId");
const type_generate_id_1 = require("../../utils/type_generate_id");
const userId = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const queryUser = `
        SELECT COUNT(*) FROM users
    `;
        const user = yield db.query(queryUser);
        const count = user.rows[0];
        return (0, generateId_1.generateId)(type_generate_id_1.typeGenerateID.user.description, count['count']);
    }
    catch (error) {
        return null;
    }
});
exports.userId = userId;
const personId = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const queryUser = `
          SELECT COUNT(*) FROM persons
      `;
        const user = yield db.query(queryUser);
        const count = user.rows[0];
        return (0, generateId_1.generateId)(type_generate_id_1.typeGenerateID.person.description, count['count']);
    }
    catch (error) {
        return null;
    }
});
exports.personId = personId;
const cityId = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const queryUser = `
            SELECT COUNT(*) FROM cities
        `;
        const user = yield db.query(queryUser);
        const count = user.rows[0];
        return (0, generateId_1.generateId)(type_generate_id_1.typeGenerateID.person.description, count['count']);
    }
    catch (error) {
        return null;
    }
});
exports.cityId = cityId;
