"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserById = void 0;
const db = require("../../config/database");
const getUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `
    SELECT  u.user_id, u.image, u.email, 
    p.name, p.p_lastname, p.s_lastname, p.phone_number, 
    r.rol, 
    s.street, 
    c.city 
    FROM users AS u 
    INNER JOIN persons AS p ON (u.person_id = p.person_id) 
    INNER JOIN roles AS r ON (u.rol_id = r.rol_id) 
    INNER JOIN strees AS s ON (s.street_id = p.street_id) 
    INNER JOIN cities AS c ON (p.city_id = c.city_id) 
    WHERE user_id = 'USR001'    
    `;
        const categories = yield db.query(query);
        const docs = categories.rows;
        if (docs.length <= 0) {
            return res.status(404).json({ ok: true, docs });
        }
        return res.status(200).json({ ok: true, doc: docs[0] });
    }
    catch (error) {
        return res.status(500).json("Interval Server error");
    }
});
exports.getUserById = getUserById;
