"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getStreetId = exports.getCitieByNameId = exports.getSearchCities = exports.getCities = void 0;
const db = require("../../config/database");
const generateId_1 = require("../../middleware/generateId");
const type_generate_id_1 = require("../../utils/type_generate_id");
const fs = require("fs-extra");
const path = require("path");
const fullPath = path.resolve("./src/api_cities_nacionality/co.json");
const getCitieByNameId = (cityName) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!cityName || cityName.length <= 0)
            return null;
        const query = `
    SELECT city_id FROM cities WHERE city LIKE $1
  `;
        const city = yield db.query(query, [`%${cityName}%`]);
        const objCity = city.rows[0];
        if (!objCity) {
            const query = `
      SELECT COUNT(*) FROM cities
    `;
            const cities = yield db.query(query, [`%${cityName}%`]);
            const count = cities.rows[0];
            const id = (0, generateId_1.generateId)(type_generate_id_1.typeGenerateID.city.description, count);
            if (!id)
                return null;
            const queryI = `
      INSERT INTO cities(
      street_id, street, city_id)
      VALUES ($1, $2, $3);
    `;
            yield db.query(queryI, [id, cityName]);
            return id;
        }
        const id = objCity["city_id"];
        return id;
    }
    catch (error) {
        return null;
    }
});
exports.getCitieByNameId = getCitieByNameId;
const getStreetId = (streeName, cityId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (streeName.length <= 0 && cityId.length <= 0)
            return null;
        const queryS = `
    SELECT street_id FROM strees WHERE 
    to_tsvector(LOWER(street)) @@  plainto_tsquery(LOWER($1))::tsquery 
   `;
        const street = yield db.query(queryS, [streeName]);
        const objStreet = street.rows[0];
        if (objStreet) {
            const id = objStreet["street_id"];
            return { id, create: false };
        }
        const query = `
    SELECT COUNT(*) FROM strees
  `;
        const citiesCount = yield db.query(query);
        const count = citiesCount.rows[0];
        if (!count)
            return null;
        const id = (0, generateId_1.generateId)(type_generate_id_1.typeGenerateID.street.description, count["count"]);
        if (!id)
            return null;
        const queryI = `
      INSERT INTO strees(
      street_id, street, city_id)
      VALUES ($1, $2, $3);
    `;
        yield db.query(queryI, [id, streeName, cityId]);
        return { id, create: true };
    }
    catch (error) {
        return null;
    }
});
exports.getStreetId = getStreetId;
const getCities = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const offset = (Number(page) - 1) * Number(limit);
        const data = yield fs.readJson(fullPath);
        const docs = data.slice(offset).slice(0, limit);
        const listDos = [];
        yield Promise.all(docs.map((resp) => __awaiter(void 0, void 0, void 0, function* () {
            listDos.push({
                city: resp.city,
            });
        })));
        function SortArray(x, y) {
            if (x.city < y.city)
                return -1;
            if (x.city > y.city)
                return 1;
            return 0;
        }
        let totalPages = Math.ceil(data.length / Number(limit));
        return res.status(200).json({
            ok: true,
            docs: listDos.sort(SortArray),
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            docs: [],
            totalPages: 0,
            page: 0,
            error: "Error del servidor",
        });
    }
});
exports.getCities = getCities;
const getSearchCities = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { valueSearch } = req.params;
        const data = yield fs.readJson(fullPath);
        const docs = data.filter((value) => {
            return value.city.toLowerCase().includes(valueSearch.toLowerCase());
        });
        const listDos = [];
        yield Promise.all(docs.map((resp) => __awaiter(void 0, void 0, void 0, function* () {
            listDos.push({
                city: resp.city,
            });
        })));
        function SortArray(x, y) {
            if (x.city < y.city)
                return -1;
            if (x.city > y.city)
                return 1;
            return 0;
        }
        return res.status(200).json({
            ok: true,
            docs: listDos.sort(SortArray),
            totalPages: 1,
            page: 1,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            docs: [],
            totalPages: 0,
            page: 0,
            error: "Error del servidor",
        });
    }
});
exports.getSearchCities = getSearchCities;
