"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.onMessage = void 0;
const onMessage = (payload, users) => {
    console.log("Mensaje", payload["to"]);
    users[payload["to"]].emit("ON_MESSAGE_RECEIBED", payload);
};
exports.onMessage = onMessage;
