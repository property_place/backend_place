"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.renewToken = exports.profile = exports.signin = exports.signup = void 0;
const db = require("../../config/database");
const generate_id_controller_1 = require("./generate_id.controller");
const cities_controllers_1 = require("./cities.controllers");
const role_controller_1 = require("./role.controller");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const { generarJWT } = require("../../helpers/jwt");
const signup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let persId = null;
    let objStreeId = null;
    try {
        const { image, email, rol, password, name, plastname, slastname, phoneNumber, street, city, } = req.body;
        // Obtiene o crea id
        persId = yield (0, generate_id_controller_1.personId)();
        const usrId = yield (0, generate_id_controller_1.userId)();
        const cityId = yield (0, cities_controllers_1.getCitieByNameId)(city);
        const rolId = yield (0, role_controller_1.getRolByName)(rol);
        if (!usrId && !persId && !cityId && !rolId) {
            return res
                .status(200)
                .json({ ok: false, error: "Interval Server error" });
        }
        objStreeId = yield (0, cities_controllers_1.getStreetId)(street, cityId);
        if (!objStreeId) {
            return res
                .status(200)
                .json({ ok: false, error: "Interval Server error" });
        }
        const { id: streeId } = objStreeId;
        const queryPerson = `
      INSERT INTO persons(
      person_id, name, p_lastname, s_lastname, phone_number, street_id, city_id)
      VALUES ($1, $2, $3, $4, $5, $6, $7);
    `;
        yield db.query(queryPerson, [
            persId,
            name,
            plastname,
            slastname,
            phoneNumber,
            streeId,
            cityId,
        ]);
        const queryUser = `
      INSERT INTO users(
      user_id, image, email, password, rol_id, person_id)
      VALUES ($1, $2, $3, $4, $5, $6);
    `;
        const salt = bcryptjs_1.default.genSaltSync();
        const newPassword = bcryptjs_1.default.hashSync(password, salt);
        yield db.query(queryUser, [
            usrId,
            image,
            email,
            newPassword,
            rolId,
            persId,
        ]);
        const token = jsonwebtoken_1.default.sign(usrId, process.env.JWT_KEY || "TEST_SECRECT");
        const doc = {
            user_id: usrId,
            image,
            email,
            rol,
            name,
            p_lastname: plastname,
            s_lastname: slastname,
            phone_number: phoneNumber,
            street,
            city,
        };
        return res.status(200).header("x-token", token).json({ ok: true, doc });
    }
    catch (error) {
        console.log(error);
        if (error == 'error: duplicate key value violates unique constraint "email"') {
            const query = `
      DELETE FROM persons
	    WHERE person_id = $1`;
            yield db.query(query, [persId]);
            if (objStreeId) {
                const { id: streeId, create } = objStreeId;
                if (create) {
                    const queryS = `
          DELETE FROM strees
          WHERE street_id = $1`;
                    yield db.query(queryS, [streeId]);
                }
            }
        }
        return res
            .status(500)
            .json({ ok: false, error: `Interval Server error, ${error}` });
    }
});
exports.signup = signup;
const signin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, password } = req.body;
        const query = `
    SELECT u.user_id, u.image, u.email, u.password, p.name, 
      p.p_lastname, p.s_lastname, p.phone_number,  
      c.city, s.street, r.rol
      FROM users AS u INNER JOIN persons AS p 
      ON ( u.person_id = p.person_id )
      INNER JOIN cities AS c 
      ON ( p.city_id = c.city_id )
      INNER JOIN strees AS s
      ON ( p.street_id = s.street_id )
      INNER JOIN roles AS r 
      ON (u.rol_id = r.rol_id)
      WHERE  to_tsvector(LOWER(email)) @@  
      plainto_tsquery(LOWER($1))::tsquery    
    `;
        const user = yield db.query(query, [email]);
        const docUser = user.rows[0];
        if (!docUser) {
            return res.status(404).json({
                ok: false,
                msg: "Email no encontrado",
            });
        }
        //validar p[asswors
        const validPassword = bcryptjs_1.default.compareSync(password, docUser.password);
        if (!validPassword) {
            return res.status(404).json({
                ok: false,
                msg: "La contraseña no es validad",
            });
        }
        //Generar mi token
        const token = yield generarJWT(docUser.user_id);
        docUser && delete docUser['password'];
        return res
            .status(200)
            .header("x-token", token)
            .json({ ok: true, doc: docUser });
    }
    catch (error) {
        console.log(error);
        return res
            .status(500)
            .json({ ok: false, error: `Interval Server error, ${error}` });
    }
});
exports.signin = signin;
const profile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `
    SELECT  u.user_id, u.image, u.email, 
      p.name, p.p_lastname, p.s_lastname, p.phone_number, 
      r.rol, 
      s.street, 
      c.city 
      FROM users AS u 
      INNER JOIN persons AS p ON (u.person_id = p.person_id) 
      INNER JOIN roles AS r ON (u.rol_id = r.rol_id) 
      INNER JOIN strees AS s ON (s.street_id = p.street_id) 
      INNER JOIN cities AS c ON (p.city_id = c.city_id) 
    WHERE user_id = 'USR001'    
    `;
        const categories = yield db.query(query);
        const docs = categories.rows;
        if (docs.length <= 0) {
            return res.status(404).json({ ok: true, docs });
        }
        return res.status(200).json({ ok: true, doc: docs[0] });
    }
    catch (error) {
        return res.status(500).json("Interval Server error");
    }
});
exports.profile = profile;
const renewToken = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `
    SELECT u.user_id, u.image, u.email, p.name, p.p_lastname, p.s_lastname, p.phone_number,  
      c.city, s.street, r.rol
      FROM users AS u INNER JOIN persons AS p 
      ON ( u.person_id = p.person_id )
      INNER JOIN cities AS c 
      ON ( p.city_id = c.city_id )
      INNER JOIN strees AS s
      ON ( p.street_id = s.street_id )
      INNER JOIN roles AS r 
      ON (u.rol_id = r.rol_id)
      WHERE u.user_id = $1    
    `;
        const user = yield db.query(query, [req.user_id]);
        const doc = user.rows[0];
        if (!doc) {
            return res.status(500).json({
                ok: false,
            });
        }
        const token = yield generarJWT(req.user_id);
        return res.status(200).header("x-token", token).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            doc: {},
        });
    }
});
exports.renewToken = renewToken;
