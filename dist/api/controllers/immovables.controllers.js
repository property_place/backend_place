"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getImmovableOffers = exports.getImmovables = exports.getImmovable = void 0;
const db = require("../../config/database");
const type_filter_1 = require("../../utils/type_filter");
const SimpleDateFormat = require("@riversun/simple-date-format");
const getImmovables = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10, category_id, city = "Tumaco" } = req.params;
        var ft = req.body;
        let order = "published_date";
        let whereCondition = "";
        const query = `
      SELECT i.immovable_id, i.description, i.price, i.published_date, 
      i.state, ii.outside_front_image FROM immovables AS i  INNER JOIN 
      immovableimages AS ii ON (i.image_id = ii.immovable_image_id) INNER JOIN
      cities AS c  ON (i.city_id = c.city_id )
    `;
        let value = [page, limit, category_id, "disponible", city];
        if (ft && ft.length > 0) {
            const orderQuery = ft.find((ftr) => ftr.key == type_filter_1.typeFilter.order.description);
            if (orderQuery) {
                const data = orderQuery.data;
                order = `i.${data.sort} ${data.type}`;
            }
            const orderQueryBetweenDate = ft.find((ft) => ft.key == type_filter_1.typeFilter.date.description);
            if (orderQueryBetweenDate) {
                const data = orderQueryBetweenDate.data;
                const elItem = value.length;
                if (data.minimum &&
                    data.minimum != "" &&
                    data.maximum &&
                    data.maximum != "") {
                    whereCondition = whereCondition.concat(`AND i.${data.sort} BETWEEN $${elItem + 1} AND $${elItem + 2} `, " ", ...whereCondition);
                    var dateMinimum = new Date(data.minimum);
                    var dateMaximum = new Date(data.maximum);
                    const sdf = new SimpleDateFormat();
                    let minimum = sdf.formatWith("dd/MM/yyyy' 'HH:mm:ss", dateMinimum);
                    let maximum = sdf.formatWith("dd/MM/yyyy' 'HH:mm:ss", dateMaximum);
                    value = [...value, minimum, maximum];
                }
            }
            const orderQueryBetweenPrice = ft.find((ft) => ft.key == type_filter_1.typeFilter.price.description);
            if (orderQueryBetweenPrice) {
                const elItem = value.length;
                const data = orderQueryBetweenPrice.data;
                if (data.minimum &&
                    data.minimum != "" &&
                    data.maximum &&
                    data.maximum != "") {
                    whereCondition = whereCondition.concat(`AND i.${data.sort} BETWEEN $${elItem + 1} AND $${elItem + 2}`, " ", ...whereCondition);
                    value = [...value, data.minimum, data.maximum];
                }
            }
        }
        const condition = `
      WHERE i.category_id = $3 AND i.state= $4 AND c.city = $5
      ${whereCondition} ORDER BY ${order} 
      LIMIT $2 OFFSET (($1 - 1) * $2);
    `;
        const consult = `${query} ${condition}`;
        const immovables = yield db.query(consult, value);
        const docs = immovables.rows;
        const docslength = docs.length;
        const totalPages = Math.ceil(docslength > Number(limit) ? docslength / Number(limit) : docs.length);
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
            error: "",
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            docs: [],
            totalPages: 0,
            page: 0,
            error: "Error del servidor",
        });
    }
});
exports.getImmovables = getImmovables;
const getImmovableOffers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10, category_id, city = "Tumaco" } = req.params;
        const query = `
       SELECT i.immovable_id, i.description, o.offer_id , o.image, 
       o.start_date, o.end_date, o.new_price, ii.outside_front_image
       FROM immovables AS i INNER JOIN offers AS o ON
       (i.immovable_id = o.immovable_id) INNER JOIN immovableimages AS ii ON
       (i.image_id = ii.immovable_image_id) INNER JOIN
       cities AS c  ON (i.city_id = c.city_id ) WHERE i.category_id = $3 AND
       c.city = $4
       ORDER BY i.description LIMIT $2 OFFSET (($1 - 1) * $2);
      `;
        const immovabkeOffers = yield db.query(query, [
            page,
            limit,
            category_id,
            city,
        ]);
        const docs = immovabkeOffers.rows;
        const docslength = docs.length;
        const totalPages = Math.ceil(docslength > Number(limit) ? docslength / Number(limit) : docs.length);
        return res
            .status(200)
            .json({ ok: true, docs, totalPages, page: Number(page), error: "" });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            docs: [],
            totalPages: 0,
            page: 0,
            error: "Error del servidor",
        });
    }
});
exports.getImmovableOffers = getImmovableOffers;
const getImmovable = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { immovableId } = req.params;
        const queryPartsHouseImages = `
        SELECT phi.parts_house_image_id, phi.parts_house_description, 
        phi.parts_house_image FROM immovables AS i
        INNER JOIN immovableimages AS ii
        ON ( i.image_id = ii.immovable_image_id )
        INNER JOIN partshouseimages AS phi
        ON (ii.immovable_image_id = phi.immovable_image_id)
        WHERE immovable_id = $1
     `;
        const immovablePartsHouseImages = yield db.query(queryPartsHouseImages, [immovableId]);
        const partsHouseCharacteristicImage = `
        SELECT phci.parts_house_characteristic_image_id, 
        phci.parts_house_characteristic, phci.parts_house_characteristic_image
        FROM immovables AS i
        INNER JOIN immovableimages AS ii
        ON ( i.image_id = ii.immovable_image_id )
        INNER JOIN partshousecharacteristicimages AS phci
        ON (ii.immovable_image_id = phci.immovable_image_id)
        WHERE immovable_id = $1
     `;
        const immovablePartsHouseCharacteristicImage = yield db.query(partsHouseCharacteristicImage, [immovableId]);
        const queryImmovableimages = `
        SELECT ii.outside_front_image
        FROM immovables AS i
        INNER JOIN immovableimages AS ii
        ON ( i.image_id = ii.immovable_image_id )
        WHERE immovable_id = $1
    `;
        const immovableimages = yield db.query(queryImmovableimages, [
            immovableId,
        ]);
        const queryImmovable = `
        SELECT immovable_id, description, characteristic, detail, state, price,
        published_date, st.street, ct.city FROM immovables AS i 
        INNER JOIN strees AS st 
        ON ( i.street_id = st.street_id ) INNER JOIN cities AS ct
        ON ( i.city_id = ct.city_id ) WHERE immovable_id = $1
      `;
        const immovable = yield db.query(queryImmovable, [
            immovableId,
        ]);
        const queryPerson = `
        SELECT u.user_id, u.image, u.email, p.name, p.p_lastname, p.s_lastname,
        imm.contract_id FROM users AS u INNER JOIN persons AS p 
        ON (u.person_id = p.person_id) INNER JOIN immovables AS imm
        ON (u.user_id = imm.user_id) WHERE immovable_id = $1
      `;
        const personImmovable = yield db.query(queryPerson, [
            immovableId,
        ]);
        const docs = {
            outsideFrontImage: immovableimages.rows[0]["outside_front_image"],
            partsHouseImages: [
                {
                    parts_house_image_id: "IMGCF001",
                    parts_house_description: "Casa frontal",
                    parts_house_image: immovableimages.rows[0]["outside_front_image"],
                },
                ...immovablePartsHouseImages.rows,
            ],
            partsHouseCharacteristicImage: immovablePartsHouseCharacteristicImage.rows,
            immovable: immovable.rows[0],
            infoPerson: personImmovable.rows[0],
        };
        // console.log(docs);
        return res.status(200).json({ ok: true, docs, error: "" });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            docs: [],
            totalPages: 0,
            page: 0,
            error: "Error del servidor",
        });
    }
});
exports.getImmovable = getImmovable;
