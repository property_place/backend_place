"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const db = new pg_1.Pool({
    user: process.env.DB_USER,
    host: "localhost",
    password: "root",
    database: process.env.DB,
    port: Number(process.env.PORT_DB),
});
module.exports = db;
