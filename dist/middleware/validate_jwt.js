"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
const validateJWT = (req, res, next) => {
    //Leer el token
    const token = req.header("x-token");
    if (!token) {
        return res.status(401).json({
            ok: false,
            error: "No hay token en la peticion",
        });
    }
    try {
        const { uid } = jwt.verify(token, process.env.JWT_KEY);
        req.user_id = uid;
        console.log('data', uid);
        next();
    }
    catch (error) {
        return res.status(401).json({
            ok: false,
            error: "Token no valido",
        });
    }
};
module.exports = {
    validateJWT,
};
