"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateId = void 0;
const generateId = (identificador, identificadorLength) => {
    const id = identificador.toUpperCase();
    const serie = Number(identificadorLength) + 1;
    return `${id}${serie}`;
};
exports.generateId = generateId;
