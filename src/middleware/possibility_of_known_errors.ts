import { errosType } from "./error_type";
import { Request, Response } from "express";
const db = require("../config/database");

const possibilityOfKnownErrors = async ({
  res,
  error,
  persId,
  objStreeId,
}: {
  req: Request;
  res: Response;
  error: any;
  persId: any;
  objStreeId: any;
}): Promise<Response> => {
  if (
    error == 'error: duplicate key value violates unique constraint "email"'
  ) {
    if (persId) {
      const query = `
          DELETE FROM persons
            WHERE person_id = $1`;
      await db.query(query, [persId]);
    }

    if (objStreeId) {
      const { id: streeId, create } = objStreeId;
      if (create) {
        const queryS = `
            DELETE FROM strees
            WHERE street_id = $1`;
        await db.query(queryS, [streeId]);
      }
    }

    return res.status(500).json({
      ok: false,
      error: errosType({ param: "Email", msg: "Correo no valido" }),
    });
  }
  return res.status(500).json({
    ok: false,
    error: errosType({ msg: "Interval Server error" }),
  });
};

module.exports = {
  possibilityOfKnownErrors,
};
