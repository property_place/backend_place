export const generateId = (
  identificador: string,
  identificadorLength: number
): string => {
  const id = identificador.toUpperCase();
  const serie = Number(identificadorLength) + 1;
  return `${id}${serie}`;
};
