import multer from "multer";
const path = require("path");

const storage =  multer.diskStorage({
  destination: path.join(__dirname, "public/uploads"),
  filename: (req, file, cb) => {
    cb(null, new Date().getTime().toString() + path.extname(file.originalname));
  }
});

exports.module = storage;