export const typeGenerateID = {
  immovable: Symbol("IMM00"),
  user: Symbol("USR00"),
  person: Symbol("PERS00"),
  city: Symbol("CT00"),
  street: Symbol("STE00"),
};
