const jwt = require("jsonwebtoken");

const generarJWT = (uid: string) => {
  return new Promise((resolve, reject) => {
    const payload = { uid };
    jwt.sign(
      payload,
      process.env.JWT_KEY,
      {
        expiresIn: "24h",
      },
      (err: any, token: string) => {
        if (err) {
          //No se pudo crear el token
          reject("No se pudo generar el JWT");
        } else {
          //Token
          resolve(token);
        }
      }
    );
  });
};

const verifyJWT = (token: string) => {
  try {
    const { uid } = jwt.verify(token, process.env.JWT_KEY);
    return [true, uid];
  } catch (error) {
    return [false, null];
  }
};

module.exports = {
  generarJWT,
  verifyJWT,
};
