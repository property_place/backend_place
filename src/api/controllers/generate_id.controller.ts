const db = require("../../config/database");
import { QueryResult } from "pg";
import { generateId } from "../../middleware/generate_id";
import { typeGenerateID } from "../../utils/type_generate_id";

const userId = async (): Promise<string | null> => {
  try {
    const queryUser = `
        SELECT COUNT(*) FROM users
    `;
    const user: QueryResult = await db.query(queryUser);
    const count = user.rows[0];
    return generateId(typeGenerateID.user.description!, count['count']);
  } catch (error) {
    return null;
  }
};

const personId = async (): Promise<string | null> => {
  try {
    const queryUser = `
          SELECT COUNT(*) FROM persons
      `;
    const user: QueryResult = await db.query(queryUser);
    const count = user.rows[0];
    return generateId(typeGenerateID.person.description!, count['count']);
  } catch (error) {
    return null;
  }
};

const cityId = async (): Promise<string | null> => {
    try {
      const queryUser = `
            SELECT COUNT(*) FROM cities
        `;
      const user: QueryResult = await db.query(queryUser);
      const count = user.rows[0];
      return generateId(typeGenerateID.person.description!, count['count']);
    } catch (error) {
      return null;
    }
  };

export { userId, personId, cityId };
