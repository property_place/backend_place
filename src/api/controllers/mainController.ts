import {
  ConnectedSocket,
  OnConnect,
  OnDisconnect,
  SocketController,
  SocketIO,
} from "socket-controllers";
import { Socket, Server } from "socket.io";
const { verifyJWT } = require("../../helpers/jwt");
const {
  connectUser,
  disconnectUser,
  saveChat,
  newOrUpdateChatUser,
  newChatImmovableUser,
  onWriting
} = require("./socket.controller");

@SocketController()
export class MainController {
  @OnConnect()
  public onConnection(
    @ConnectedSocket() socket: Socket,
    @SocketIO() io: Server
  ) {
    console.log("New Socket connected: ", socket.id);
    const [valid, uid] = verifyJWT(
      socket.handshake.headers["x-token"]?.toString() ?? ""
    );
    // Verificando autenticacion
    if (!valid) {
      return socket.disconnect();
    }

    // Cliente conectado
    connectUser(uid);

    // Socket, enviamos el mensaje al servidor
    // para ser agregado a la lista de mensajes del chat
    socket.join(uid);

    // Escuchar del cliente el on_message_personal
    socket.on("on_message_personal", async (payload) => {
      // add list new chat user
      await saveChat(payload);
      // add list new chat user
      io.to(payload.receiverID).emit("on_message_personal", payload);
      // add list new chat user
      await onWriting(io, payload);

      // add list new chat user
      await newChatImmovableUser(io, payload);
      // add list new chat user
      await newOrUpdateChatUser(io, payload);

    });

    socket.on("on_writing", (payload) => {
      io.to(payload.receiverID).emit("on_writing", payload);
    });
  }
  @OnDisconnect()
  disconnect(@ConnectedSocket() socket: Socket) {
    const [valid, uid] = verifyJWT(
      socket.handshake.headers["x-token"]?.toString() ?? ""
    );
    console.log("client disconnected", socket.id);
    disconnectUser(uid);
  }
}
