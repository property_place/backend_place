import { QueryResult } from "pg";
const db = require("../../config/database");
import { Server } from 'socket.io';

const immovableimages = async () => {
    //INSERT
     const q = `
      UPDATE immovableimages
          SET parts_house_images = array_append(parts_house_images, $2::JSONB)
         WHERE immovable_image_id = $1;
    
      `;
      await db.query(q, [
        'IIMG001',
        {parts_house_image_id: 'PHI001', parts_house_description: 'Sala', parts_house_image: 'https://www.vitahome.co/wp-content/uploads/2018/04/sala-L-essenze.jpg'},
        {parts_house_image_id: 'PHI002', parts_house_description: 'Cosina', parts_house_image: 'https://i.pinimg.com/originals/ce/2f/b2/ce2fb2bb5ab6379ae9af37d90ebf0137.jpg'},
        {parts_house_image_id: 'PHI003', parts_house_description: 'Habitacion 1', parts_house_image: 'https://cdn.shortpixel.ai/spai/w_1055+q_lossy+ex_1+ret_img+to_webp/https://adaziodesign.com/wp-content/uploads/2020/12/habitaciones-juveniles-adazio-1200x1200.jpg'},
        {parts_house_image_id: 'PHI004', parts_house_description: 'Habitacion 2', parts_house_image: 'https://i.pinimg.com/originals/13/35/ca/1335ca8334e5eeb761d595deaac1458d.jpg'},
        {parts_house_image_id: 'PHI005', parts_house_description: 'Patio', parts_house_image: 'https://espacioscreativo.com/wp-content/uploads/2020/06/ideas-para-el-patio-de-tu-casa-4.jpg'}
      ]);
    
    //UPDATE
    
    // const q = `
    // UPDATE contracts
    // 	SET visibility_for_user = array_append(visibility_for_user, $1::JSONB)
    // 	WHERE contract_id = $2;
    // `;
    //   await db.query(q, [
    //      {user_id: 'USR001'},
    //      'CONT002',
    //   ]);
    
    
    // POR RVIZAR
    
    // const q = `
    // SELECT oontract_id, array_agg(j) AS j_new
    // FROM   contracts, unnest(jar) AS j   -- LATERAL JOIN
    // WHERE  j->>'user_id' <> $1
    // GROUP  BY 1;
    
    //     `;
    //   await db.query(q, [
    //      'USR001',
    //   ]);
    
    
    // const q = `
      
    // UPDATE contracts
    // SET visibility_for_user = jsonb_set(visibility_for_user, '{info}', (
    //   SELECT COALESCE(jsonb_agg(element), '[]'::jsonb)
    //   FROM jsonb_array_elements(visibility_for_user -> 'info') element
    //   WHERE element ->> 'user_id' <> '$1'
    // ))
      
    // `;
      
    //   await db.query(q, [
    //      'USR001',
    //   ]);
    
    }


    const partshousecharacteristicimages = async () => {
        //INSERT
         const q = `
          UPDATE partshousecharacteristicimages
              SET parts_house_characteristic_image = array_append(parts_house_characteristic_image, $2::JSONB)
             WHERE parts_house_characteristic_image_id = $1;
          `;
          await db.query(q, [
            'IIMG001',
            {parts_house_characteristic_image_id: 'PHCII001', parts_house_characteristic: 'Camas', parts_house_characteristic_image: '/public/resources/immovables/bedroom.svg'},           
            {parts_house_characteristic_image_id: 'PHCII002', parts_house_characteristic: '1 Cosina', parts_house_characteristic_image: '/public/resources/immovables/kitchen.svg'},
            {parts_house_characteristic_image_id: 'PHCII003', parts_house_characteristic: '2 Close', parts_house_characteristic_image: '/public/resources/immovables/floor.svg'},
            {parts_house_characteristic_image_id: 'PHCII004', parts_house_characteristic: '120 Metros', parts_house_characteristic_image: '/public/resources/immovables/ruler.svg'}
        ]);
        
        //UPDATE
        
        // const q = `
        // UPDATE contracts
        // 	SET visibility_for_user = array_append(visibility_for_user, $1::JSONB)
        // 	WHERE contract_id = $2;
        // `;
        //   await db.query(q, [
        //      {user_id: 'USR001'},
        //      'CONT002',
        //   ]);
        
        
        // POR RVIZAR
        
        // const q = `
        // SELECT oontract_id, array_agg(j) AS j_new
        // FROM   contracts, unnest(jar) AS j   -- LATERAL JOIN
        // WHERE  j->>'user_id' <> $1
        // GROUP  BY 1;
        
        //     `;
        //   await db.query(q, [
        //      'USR001',
        //   ]);
        
        
        // const q = `
          
        // UPDATE contracts
        // SET visibility_for_user = jsonb_set(visibility_for_user, '{info}', (
        //   SELECT COALESCE(jsonb_agg(element), '[]'::jsonb)
        //   FROM jsonb_array_elements(visibility_for_user -> 'info') element
        //   WHERE element ->> 'user_id' <> '$1'
        // ))
          
        // `;
          
        //   await db.query(q, [
        //      'USR001',
        //   ]);
        
        }