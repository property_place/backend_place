import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import {
  Filters,
  DataFilters,
  DataFiltersDateOrPrice,
} from "models/filters_interface";
import { typeFilter } from "../../utils/type_filter";
import { errosType } from "../../middleware/error_type";
const SimpleDateFormat = require("@riversun/simple-date-format");

const getImmovables = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10, category_id, city = "Tumaco" } = req.params;

    var ft: Filters[] = req.body;

    let order = "published_date";
    let whereCondition = "";

    const query = `
      SELECT i.immovable_id, i.description, i.price, i.published_date, 
      i.state, ii.outside_front_image FROM immovables AS i  INNER JOIN 
      immovableimages AS ii ON (i.image_id = ii.immovable_image_id) INNER JOIN
      cities AS c  ON (i.city_id = c.city_id )
    `;

    let value = [page, limit, category_id, "disponible", city];

    if (ft && ft.length > 0) {
      const orderQuery = ft.find(
        (ftr) => ftr.key == typeFilter.order.description
      );

      if (orderQuery) {
        const data = orderQuery.data as DataFilters;
        order = `i.${data.sort} ${data.type}`;
      }

      const orderQueryBetweenDate = ft.find(
        (ft) => ft.key == typeFilter.date.description
      );

      if (orderQueryBetweenDate) {
        const data = orderQueryBetweenDate.data as DataFiltersDateOrPrice;
        const elItem = value.length;
        if (
          data.minimum &&
          data.minimum != "" &&
          data.maximum &&
          data.maximum != ""
        ) {
          whereCondition = whereCondition.concat(
            `AND i.${data.sort} BETWEEN $${elItem + 1} AND $${elItem + 2} `,
            " ",
            ...whereCondition
          );

          var dateMinimum = new Date(data.minimum);
          var dateMaximum = new Date(data.maximum);

          const sdf = new SimpleDateFormat();
          let minimum = sdf.formatWith("dd/MM/yyyy' 'HH:mm:ss", dateMinimum);
          let maximum = sdf.formatWith("dd/MM/yyyy' 'HH:mm:ss", dateMaximum);

          value = [...value, minimum, maximum];
        }
      }

      const orderQueryBetweenPrice = ft.find(
        (ft) => ft.key == typeFilter.price.description
      );

      if (orderQueryBetweenPrice) {
        const elItem = value.length;
        const data = orderQueryBetweenPrice.data as DataFiltersDateOrPrice;
        if (
          data.minimum &&
          data.minimum != "" &&
          data.maximum &&
          data.maximum != ""
        ) {
          whereCondition = whereCondition.concat(
            `AND i.${data.sort} BETWEEN $${elItem + 1} AND $${elItem + 2}`,
            " ",
            ...whereCondition
          );
          value = [...value, data.minimum, data.maximum];
        }
      }
    }

    const condition = `
      WHERE i.category_id = $3 AND i.state= $4 AND c.city = $5
      ${whereCondition} ORDER BY ${order} 
      LIMIT $2 OFFSET (($1 - 1) * $2);
    `;

    const consult = `${query} ${condition}`;

    const immovables: QueryResult = await db.query(consult, value);
    const docs = immovables.rows;
    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

const getImmovableOffers = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10, category_id, city = "Tumaco" } = req.params;
    const query = `
       SELECT i.immovable_id, i.description, o.offer_id , o.image, 
       o.start_date, o.end_date, o.new_price, ii.outside_front_image
       FROM immovables AS i INNER JOIN offers AS o ON
       (i.immovable_id = o.immovable_id) INNER JOIN immovableimages AS ii ON
       (i.image_id = ii.immovable_image_id) INNER JOIN
       cities AS c  ON (i.city_id = c.city_id ) WHERE i.category_id = $3 AND
       c.city = $4
       ORDER BY i.description LIMIT $2 OFFSET (($1 - 1) * $2);
      `;
    const immovabkeOffers: QueryResult = await db.query(query, [
      page,
      limit,
      category_id,
      city,
    ]);
    const docs = immovabkeOffers.rows;
    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );

    return res
      .status(200)
      .json({ ok: true, docs, totalPages, page: Number(page) });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

const getImmovable = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { immovableId } = req.params;

    const q = `
    SELECT row_to_json(imm) as immovable
    from(
      SELECT i.immovable_id, i.description, i.characteristic, i.detail, i.state, i.price,
      i.published_date, st.street, ct.city,
        (
          SELECT row_to_json(alb) from ( select * from immovableimages where immovable_image_id = i.image_id ) AS alb
        ) as immovable_images,
        (
          SELECT row_to_json(autor) from (  SELECT u.user_id, u.image, u.email, u.online, u.name, u.p_lastname, 
          u.s_lastname, u.rol, imm.contract_id FROM info_user AS u INNER JOIN immovables AS imm
          ON (u.user_id = imm.user_id) WHERE immovable_id = i.immovable_id) AS autor
        ) AS creator
      FROM immovables AS i
      JOIN strees AS st  ON ( i.street_id = st.street_id ) 
      JOIN cities AS ct ON ( i.city_id = ct.city_id )
      WHERE i.immovable_id = $1
    ) imm;
    `;

    const immovablePartsHouseImages: QueryResult = await db.query(q, [
      immovableId,
    ]);

    const doc = immovablePartsHouseImages.rows[0];
    
    if (doc && doc?.immovable?.immovable_images.parts_house_images) {
      doc.immovable.immovable_images.parts_house_images.unshift({
        parts_house_image: doc.immovable.immovable_images.outside_front_image,
        parts_house_image_id: doc.immovable.immovable_images.immovable_image_id,
        parts_house_description: "Casa frontal",
      });
    }

    if(doc && doc.immovable.creator){
      doc.immovable.creator = { 
        immovable_id: doc.immovable.immovable_id,
        ...doc.immovable.creator 
      }
    }

    return res.status(200).json({ ok: true, doc });
  } catch (error) {    
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

export { getImmovable, getImmovables, getImmovableOffers };
