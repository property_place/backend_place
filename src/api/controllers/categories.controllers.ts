import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import { errosType } from "../../middleware/error_type";

const getCategories = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const query = `
     SELECT * FROM categories 
     ORDER BY "name"
    `;
    const categories: QueryResult = await db.query(query);
    const docs = categories.rows;

    return res.status(200).json({ ok: true, docs });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });  }
};

export { getCategories };
