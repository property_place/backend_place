import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import { personId, userId } from "./generate_id.controller";
import { getCitieByNameId, getStreetId } from "./cities.controllers";

const getRolByName = async (rolName: string): Promise<string | null> => {
  try {
    if (rolName.length <= 0) return null;
    const query = `
    SELECT rol_id FROM roles WHERE  
    to_tsvector(LOWER(rol)) @@  plainto_tsquery(LOWER($1))::tsquery 
  `;
    const role: QueryResult = await db.query(query, [rolName]);
    const rol = role.rows[0];   
     
    if (!rol) {
      return "ROL002";
    }
    const id = rol['rol_id']
    return id;
  } catch (error) {
    return "ROL002";
  }
};

export { getRolByName };
