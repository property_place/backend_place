import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";

import { CitiesModel } from "models/cities_model";
import { generateId } from "../../middleware/generate_id";
import { typeGenerateID } from "../../utils/type_generate_id";
import { errosType } from "../../middleware/error_type";
const fs = require("fs-extra");

const path = require("path");
const fullPath = path.resolve("./src/data_local/co.json");

const getCitieByNameId = async (cityName: string): Promise<string | null> => {
  try {
    if (!cityName && cityName.length <= 0) return null;

    const query = `
    SELECT city_id FROM cities WHERE to_tsvector(LOWER(city)) @@  
    plainto_tsquery(LOWER($1))::tsquery  
  `;
    const city: QueryResult = await db.query(query, [cityName]);
    const objCity = city.rows[0];

    if (!objCity) {      
      const query = `
      SELECT COUNT(*) FROM cities
    `;
      const cities: QueryResult = await db.query(query);
      const count = cities.rows[0];

      const id = generateId(typeGenerateID.city.description!, count['count']);

      if (!id) return null;

      const queryI = `
      INSERT INTO cities(city_id, city)
      VALUES ($1, $2);
    `;

      await db.query(queryI, [id, cityName]);

      return id;
    }

    const id = objCity["city_id"];

    return id;
  } catch (error) {
    return null;
  }
};

const getStreetId = async (
  streeName: string,
  cityId: string
): Promise<{
  id: any;
  create: boolean;
} | null> => {
  try {
    if (streeName && cityId && streeName.length <= 0 && cityId.length <= 0)
      return null;

    const queryS = `
    SELECT street_id FROM strees WHERE 
    to_tsvector(LOWER(street)) @@  plainto_tsquery(LOWER($1))::tsquery 
   `;
    const street: QueryResult = await db.query(queryS, [streeName]);
    const objStreet = street.rows[0];

    if (objStreet) {
      const id = objStreet["street_id"];
      return { id, create: false };
    }

    const query = `
    SELECT COUNT(*) FROM strees
  `;
    const citiesCount: QueryResult = await db.query(query);
    const count = citiesCount.rows[0];

    if (!count) return null;

    const id = generateId(typeGenerateID.street.description!, count["count"]);

    if (!id) return null;

    const queryI = `
      INSERT INTO strees(
      street_id, street, city_id)
      VALUES ($1, $2, $3);
    `;

    await db.query(queryI, [id, streeName, cityId]);
    return { id, create: true };
  } catch (error) {
    return null;
  }
};

const getCities = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const offset = (Number(page) - 1) * Number(limit);

    const data = await fs.readJson(fullPath);
    const docs = data.slice(offset).slice(0, limit);

    const listDos: any[] = [];

    await Promise.all(
      docs.map(async (resp: CitiesModel) => {
        listDos.push({
          city: resp.city,
        });
      })
    );

    function SortArray(x: CitiesModel, y: CitiesModel) {
      if (x.city < y.city) return -1;
      if (x.city > y.city) return 1;
      return 0;
    }
    let totalPages = Math.ceil(data.length / Number(limit));

    return res.status(200).json({
      ok: true,
      docs: listDos.sort(SortArray),
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

const getSearchCities = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { valueSearch } = req.params;

    const data = await fs.readJson(fullPath);
    const docs = data.filter((value: CitiesModel) => {
      return value.city.toLowerCase().includes(valueSearch.toLowerCase());
    });
    const listDos: any[] = [];

    await Promise.all(
      docs.map(async (resp: CitiesModel) => {
        listDos.push({
          city: resp.city,
        });
      })
    );

    function SortArray(x: CitiesModel, y: CitiesModel) {
      if (x.city < y.city) return -1;
      if (x.city > y.city) return 1;
      return 0;
    }

    return res.status(200).json({
      ok: true,
      docs: listDos.sort(SortArray),
      totalPages: 1,
      page: 1,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

export { getCities, getSearchCities, getCitieByNameId, getStreetId };
