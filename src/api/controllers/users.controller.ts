import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import { errosType } from "../../middleware/error_type";

const getUserById = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `
    SELECT  u.user_id, u.image, u.email, 
    p.name, p.p_lastname, p.s_lastname, p.phone_number, 
    r.rol, s.street, c.city FROM users AS u 
    INNER JOIN persons AS p ON (u.person_id = p.person_id) 
    INNER JOIN roles AS r ON (u.rol_id = r.rol_id) 
    INNER JOIN strees AS s ON (s.street_id = p.street_id) 
    INNER JOIN cities AS c ON (p.city_id = c.city_id) 
    WHERE user_id = 'USR001'    
    `;
    const categories: QueryResult = await db.query(query);
    const docs = categories.rows;

    if (docs.length <= 0) {
      return res.status(404).json({ ok: true, docs });
    }
    return res.status(200).json({ ok: true, doc: docs[0] });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

export { getUserById };
