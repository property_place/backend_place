import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import { personId, userId } from "./generate_id.controller";
import { getCitieByNameId, getStreetId } from "./cities.controllers";
import { getRolByName } from "./role.controller";
import bcrypt from "bcryptjs";
import { User } from "models/user.interfcae";
import { errosType } from "../../middleware/error_type";
import { typeRole } from "../../utils/type_role";
const { generarJWT } = require("../../helpers/jwt");
const {
  possibilityOfKnownErrors,
} = require("../../middleware/possibility_of_known_errors");

const signup = async (req: Request, res: Response): Promise<Response> => {
  let persId = null;
  let objStreeId = null;

  try {
    const {
      image,
      email,
      rol = typeRole.spark.description,
      password,
      name,
      plastname,
      slastname,
      phoneNumber,
      street,
      city,
      online,
    } = req.body;

    const verifyQuery = `
      SELECT * FROM users WHERE 
      to_tsvector(LOWER(email)) @@  plainto_tsquery(LOWER($1))::tsquery 
    `;
    const user: QueryResult = await db.query(verifyQuery, [email]);
    const existEmail: User = user.rows[0];

    if (existEmail) {
      return res.status(400).json({
        ok: false,
        error: errosType({ msg: "El correo ya se encuentra registrado" }),
      });
    }

    // Obtiene o crea id
    persId = await personId();
    const usrId = await userId();
    const cityId = await getCitieByNameId(city);

    const rolId = await getRolByName(rol);

    if (!usrId && !persId && !cityId && !rolId) {
      return res.status(400).json({
        ok: false,
        error: errosType({ msg: "Interval Server error" }),
      });
    }

    objStreeId = await getStreetId(street ?? "", cityId!);

    if (!objStreeId) {
      return res.status(400).json({
        ok: false,
        error: errosType({ msg: "Interval Server error" }),
      });
    }
    const { id: streeId } = objStreeId;

    const queryPerson = `
        INSERT INTO persons(
        person_id, name, p_lastname, s_lastname, phone_number, street_id, city_id)
        VALUES ($1, $2, $3, $4, $5, $6, $7);
      `;

    await db.query(queryPerson, [
      persId,
      name,
      plastname,
      slastname,
      phoneNumber,
      streeId,
      cityId,
    ]);

    const queryUser = `
      INSERT INTO users(
      user_id, image, email, password, online, rol_id, person_id)
      VALUES ($1, $2, $3, $4, $5, $6, $7);
    `;

    const salt = bcrypt.genSaltSync();
    const newPassword = bcrypt.hashSync(password, salt);

    await db.query(queryUser, [
      usrId,
      image,
      email,
      newPassword,
      online,
      rolId,
      persId,
    ]);

    const token: string = await generarJWT(usrId);

    const doc = {
      user_id: usrId,
      image,
      email,
      rol,
      name,
      p_lastname: plastname,
      s_lastname: slastname,
      phone_number: phoneNumber,
      street,
      city,
    };

    return res.status(200).header("x-token", token).json({ ok: true, doc });
  } catch (error) {
    return possibilityOfKnownErrors({ res, error, persId, objStreeId });
  }
};

const signin = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { email, password } = req.body;

    const query = `SELECT * FROM signin($1)`;

    const user: QueryResult = await db.query(query, [email]);
    const docUser: User = user.rows[0];

    if (!docUser) {
      return res.status(404).json({
        ok: false,
        error: errosType({
          param: "Email",
          msg: "Credenciales invalidas (Email), por favor reviza tu informacion",
        }),
      });
    }

    //validar p[asswors
    const validPassword = bcrypt.compareSync(password, docUser.password!);

    if (!validPassword) {
      return res.status(404).json({
        ok: false,
        error: errosType({
          param: "Password",
          msg: "La contraseña no es válida",
        }),
      });
    }

    //Generar mi token
    const token = await generarJWT(docUser.user_id);

    docUser && delete docUser["password"];

    return res
      .status(200)
      .header("x-token", token)
      .json({ ok: true, doc: docUser });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const profile = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `
    SELECT  u.user_id, u.image, u.email, 
      p.name, p.p_lastname, p.s_lastname, p.phone_number, 
      r.rol, 
      s.street, 
      c.city 
      FROM users AS u 
      INNER JOIN persons AS p ON (u.person_id = p.person_id) 
      INNER JOIN roles AS r ON (u.rol_id = r.rol_id) 
      INNER JOIN strees AS s ON (s.street_id = p.street_id) 
      INNER JOIN cities AS c ON (p.city_id = c.city_id) 
    WHERE user_id = 'USR001'    
    `;
    const categories: QueryResult = await db.query(query);
    const docs = categories.rows;

    if (docs.length <= 0) {
      return res.status(404).json({ ok: true, docs });
    }
    return res.status(200).json({ ok: true, doc: docs[0] });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const renewToken = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `
    SELECT u.user_id, u.image, u.email, p.name, p.p_lastname, p.s_lastname, 
    p.phone_number, c.city, s.street, r.rol FROM users AS u 
    INNER JOIN persons AS p ON ( u.person_id = p.person_id )
    INNER JOIN cities AS c  ON ( p.city_id = c.city_id )
    INNER JOIN strees AS s  ON ( p.street_id = s.street_id )
      INNER JOIN roles AS r 
      ON (u.rol_id = r.rol_id)
      WHERE u.user_id = $1    
    `;

    const user: QueryResult = await db.query(query, [req.user_id]);
    const doc = user.rows[0];

    if (!doc) {
      return res.status(500).json({
        ok: false,
      });
    }

    const token: string = await generarJWT(req.user_id);

    return res.status(200).header("x-token", token).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      doc: {},
    });
  }
};

export { signup, signin, profile, renewToken };
