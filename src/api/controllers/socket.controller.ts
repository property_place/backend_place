import { QueryResult } from "pg";
const db = require("../../config/database");
import { Server } from "socket.io";

const connectUser = async (uid: string) => {
  try {
    const query = `
        UPDATE users
        SET online=$2
        WHERE user_id=$1
        RETURNING *;
    `;
    const resultUser: QueryResult = await db.query(query, [uid, true]);
    const user = resultUser.rows[0];
    return user;
  } catch (error) {}
};

const disconnectUser = async (uid: string) => {
  try {
    const query = `
        UPDATE users
        SET online=$2
        WHERE user_id=$1
        RETURNING *;
      `;
    const resultUser: QueryResult = await db.query(query, [uid, false]);
    const user = resultUser.rows[0];

    return user;
  } catch (error) {}
};

const saveChat = async (payload: any) => {
  /*
      payload 
       {
          chatID: '',
          receiverID:'',
          senderID:'',
          message:'',
          time: '',
          immovableID: ''
        }
     */
  try {
    const time_msg = new Date(payload["time"]).valueOf();    

    const query = `
        INSERT INTO chatmessages(
          chat_id, receiver_id, sender_id, message, time, immovable_id, time_msg)
          VALUES ($1, $2, $3, $4, $5, $6, $7)
      `;
    await db.query(query, [
      payload["chatID"],
      payload["receiverID"],
      payload["senderID"],
      payload["message"],
      payload["time"],
      payload["immovableID"],
      time_msg,
    ]);

    console.log(payload, time_msg);
    

    return true;
  } catch (error) {
    console.log('AQUI =>', error);
    
    return false;
  }
};

const newChatImmovableUser = async (io: Server, payload: any) => {
  const query = `SELECT * FROM newChatImmovable($1, $2)`;
  const rChatUsersForImmovables: QueryResult = await db.query(query, [
    payload["immovableID"],
    payload["senderID"],
  ]);
  const doc = rChatUsersForImmovables.rows[0];
  if (doc) {
    io.to(payload.receiverID).emit("on_new_chat_imm_user", doc);
  }
};

const newOrUpdateChatUser = async (io: Server, payload: any) => {
  const query = `SELECT * FROM newOrUpdateChatUser($1, $2)`;
  const rChatUsersForImmovables: QueryResult = await db.query(query, [
    payload['immovableID'],
    payload["senderID"],
  ]);
  const docs = rChatUsersForImmovables.rows[0];
  const doc =
    docs && docs["type"] === 0
      ? {
          ...docs,
        }
      : docs && docs["type"] === 1
      ? {
        type: docs['type'],
        user_id: docs["user_id"],
        last_message: docs["last_message"],
      }
      : null;
  if(docs == null) return;   
  io.to(payload.receiverID).emit("on_new_or_update_chat_user", doc);
};

const onWriting = (io: Server, payload: any) => {
  io.to(payload.receiverID).emit("on_writing", {
    writing: false,
    chatID: payload["chatID"],
    receiverID: payload["receiverID"],
  });
};

const upUI = () => {};

module.exports = {
  connectUser,
  disconnectUser,
  saveChat,
  newOrUpdateChatUser,
  newChatImmovableUser,
  onWriting,
  upUI,
};
