import { Request, Response } from "express";
const db = require("../../config/database");
import { QueryResult } from "pg";
import { errosType } from "../../middleware/error_type";

/**
 * Obtiene los inmuebles que le hallan escrito en chat, con el usuario @userID
 */

const getChatForInmuebleById = async (req: Request, res: Response) => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const userID = req.user_id;
    const query = `
      SELECT imm.immovable_id, imm.state, imm.description, img.outside_front_image, 
      COUNT(ids.user_id) as quantity FROM immovables AS imm  
      JOIN (
        SELECT u.user_id, imm.immovable_id,  imm.state, imm.description FROM chatmessages AS chat 
        JOIN users AS u  ON ( (u.user_id = chat.sender_id) OR (u.user_id = chat.receiver_id))
        JOIN immovables AS imm  ON ( chat.immovable_id = imm.immovable_id )
        WHERE u.user_id != imm.user_id GROUP BY u.user_id, imm.immovable_id, imm.state, imm.description
      ) AS ids
      ON ( ids.immovable_id = imm.immovable_id )
      INNER JOIN immovableimages AS img ON ( imm.image_id = img.immovable_image_id )
      WHERE imm.user_id = $3
      GROUP BY imm.immovable_id, img.outside_front_image 
      ORDER BY imm.description LIMIT $2 OFFSET (($1 - 1) * $2);
    `;
    const rChatForImmovables: QueryResult = await db.query(query, [
      page,
      limit,
      userID,
    ]);

    const docs = rChatForImmovables.rows;
    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );
    return res
      .status(200)
      .json({ ok: true, docs, totalPages, page: Number(page) });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

/**
 * Obtiene los usuarios que hallan escrito en chat, al usuario @userID
 * clasificado por inmueble con el identificador @immovableId
 */

const getChatUserForInmueblebyId = async (req: Request, res: Response) => {
  try {
    const { page = 1, limit = 10, immovableId } = req.params;
    const userID = req.user_id;

    const query = `SELECT * FROM get_chat_user_for_property_by_id($3, $4)
                   LIMIT $2 OFFSET (($1 - 1) * $2);
    `;
    const rChatUsersForImmovables: QueryResult = await db.query(query, [
      page,
      limit,
      immovableId,
      userID,
    ]);
    const docs = rChatUsersForImmovables.rows;    

    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );

    return res
      .status(200)
      .json({ ok: true, docs, totalPages, page: Number(page) });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

/**
 * Obtenemos los usuarioos que hallan escrito en chat identificado
 * con el id @userID por cada chat
 */

const getImmInQuiryChatUsersById = async (req: Request, res: Response) => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const userID = req.user_id;
    //TODO: revisar
    const query = `
     SELECT u.user_id, u.email, u.image, r.rol, p.name,  p.p_lastname, p.s_lastname, u.online,
     img.outside_front_image, imm.immovable_id, imm.description as immovable, imm.state,
     substring(chat.message for 30) AS last_message, chat.chat_id, chat.time  FROM chatmessages AS chat 
     INNER JOIN 
      (
        SELECT u.user_id, chat.immovable_id, MAX(time) max_time
        FROM chatmessages AS chat
        JOIN users AS u ON ( (u.user_id = chat.sender_id) OR u.user_id = chat.receiver_id )
        JOIN immovables AS imm  ON ( chat.immovable_id = imm.immovable_id )
        WHERE u.user_id != imm.user_id 
        GROUP BY u.user_id, chat.immovable_id
      ) AS ct
      ON chat.time = ct.max_time
      JOIN immovables AS imm  ON ( ct.immovable_id = imm.immovable_id )
      JOIN users AS u ON (u.user_id = imm.user_id)
      JOIN roles AS r ON (u.rol_id = r.rol_id)
      JOIN persons AS p ON (u.person_id = p.person_id)
      JOIN immovableimages AS img ON ( imm.image_id = img.immovable_image_id )
      WHERE ct.user_id = $3
      ORDER BY p.name LIMIT $2 OFFSET (($1 - 1) * $2);
    `;
    const rChatUsersForImmovables: QueryResult = await db.query(query, [
      page,
      limit,
      userID,
    ]);
    const docs = rChatUsersForImmovables.rows;
    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );

    return res
      .status(200)
      .json({ ok: true, docs, totalPages, page: Number(page) });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

/**
 * Obtenemos los chat guardados identificado con el id @senderID
 * por cada chat
 */

const getChats = async (req: Request, res: Response) => {
  try {
    const { page = 1, limit = 30, senderID } = req.params;
    const user_id = req.user_id;

    const query = `
        SELECT 
        chat_id, receiver_id, sender_id, message, to_char(time, 'HH24:MI AM') AS time, 
        immovable_id, time_msg
        FROM chatmessages WHERE sender_id = $3 AND receiver_id = $4 OR
        sender_id = $4 AND receiver_id = $3
        ORDER BY time DESC LIMIT $2 OFFSET (($1 - 1) * $2);
    `;
    const rChatUsersForImmovables: QueryResult = await db.query(query, [
      page,
      limit,
      user_id,
      senderID,
    ]);
    const docs = rChatUsersForImmovables.rows;
    const docslength = docs.length;
    const totalPages = Math.ceil(
      docslength > Number(limit) ? docslength / Number(limit) : docs.length
    );

    return res
      .status(200)
      .json({ ok: true, docs, totalPages, page: Number(page) });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
};

const activeContractForUser = async (req: Request, res: Response) => {
  try {
   const { userId, contractId } = req.body;
    const query = `
    UPDATE contracts
        SET visibility_for_user = array_append(visibility_for_user, $2::JSONB)
        WHERE contract_id = $1;
    `;
    await db.query(query, [
      contractId,
      {user_id: userId},
    ]);
    return res
    .status(200)
    .json({ ok: true, msg: 'Le has dado permiso, para visualizar el contracto al usuario.' });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({ msg: "Interval Server error" }),
    });
  }
}



module.exports = {
  getChatForInmuebleById,
  getChatUserForInmueblebyId,
  getChats,
  getImmInQuiryChatUsersById,
  activeContractForUser
};
