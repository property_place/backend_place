import { Router } from "express";
import { getCities, getSearchCities } from "../api/controllers/cities.controllers";

const router = Router();

router.get("/getCities/:page/:limit/", getCities);
router.get("/getSearchCities/:valueSearch", getSearchCities);


module.exports = router;