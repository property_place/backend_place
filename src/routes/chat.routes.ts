import { Router } from "express";
const {
  getChatForInmuebleById,
  getChatUserForInmueblebyId,
  getChats,
  getImmInQuiryChatUsersById,
  activeContractForUser,
} = require("../api/controllers/chat.controller");
const { validateJWT } = require("../middleware/validate_jwt");
const router = Router();

router.get(
  "/getChatForImmovable/:page/:limit",
  validateJWT,
  getChatForInmuebleById
);
router.get(
  "/getChatUserForImmovable/:page/:limit/:immovableId",
  validateJWT,
  getChatUserForInmueblebyId
);
router.get("/getChats/:page/:limit/:senderID", validateJWT, getChats);

router.get(
  "/getImmInQuiryChatUsers/:page/:limit",
  validateJWT,
  getImmInQuiryChatUsersById
);

router.post("/giveAccessToTheContract", validateJWT, activeContractForUser);

module.exports = router;
