/* 

    path : 'api/auth'

*/

import { Router } from "express";
import { signin, signup, profile, renewToken } from "../api/controllers/auth.controller";
const { check } = require("express-validator");
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require('../middleware/validate_jwt');

const router = Router();

router.post(
  "/signin",
  [
    check("email", "El email es obligatorio").isEmail(),
    check("password", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
  ],
  signin
);
router.post(
  "/signup",
  [
    check("name", "El nombre es obligatorio").not().isEmpty(),
    check("email", "El email es obligatorio").not().isEmpty(),
    check("email", "El email no es valido").isEmail(),
    check("password", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
  ],
  signup
);
router.get("/getUserById/:userId", profile);

router.get('/renew', validateJWT, renewToken)


module.exports = router;