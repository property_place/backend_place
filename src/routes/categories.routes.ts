import { Router } from "express";
import { getCategories } from "../api/controllers/categories.controllers";

const router = Router();

router.get("/getCategories", getCategories);

module.exports = router;