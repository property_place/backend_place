import { Router } from "express";
import { getImmovable, getImmovableOffers, getImmovables } from "../api/controllers/immovables.controllers";
const router = Router();

router.get("/getImmovable/:immovableId", getImmovable);
router.post("/getImmovables/:page/:limit/:category_id/:city", getImmovables);
router.get("/getImmovableOffers/:page/:limit/:category_id/:city", getImmovableOffers);

module.exports = router;