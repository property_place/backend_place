import { Pool } from "pg";

const db = new Pool({
  user: process.env.DB_USER,
  host: "localhost",
  password: "root",
  database: process.env.DB,
  port: Number(process.env.PORT_DB),
});

module.exports = db;
