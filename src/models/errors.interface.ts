export interface Errors {
  value?: string;
  msg: string;
  param?: string;
  location?: string;
}
