export interface Image {
  title: string;
  description: string;
  imageUrl: string;
  public_id: string;
}
