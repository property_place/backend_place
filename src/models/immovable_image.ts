export interface immovableImage {
  parts_house_image_id: string;
  parts_house_description: string;
  parts_house_image: string;
}
