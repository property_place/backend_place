export interface CitiesModel {
  city: string;
  lat: string;
  lng: string;
  country: Country;
  iso2: Iso2;
  admin_name: AdminName;
  capital: Capital;
  population: string;
  population_proper: string;
}

export enum AdminName {
  Amazonas = "Amazonas",
  Antioquia = "Antioquia",
  Arauca = "Arauca",
  Atlántico = "Atlántico",
  Bogotá = "Bogotá",
  Bolívar = "Bolívar",
  Boyacá = "Boyacá",
  Caldas = "Caldas",
  Caquetá = "Caquetá",
  Casanare = "Casanare",
  Cauca = "Cauca",
  Cesar = "Cesar",
  Chocó = "Chocó",
  Cundinamarca = "Cundinamarca",
  Córdoba = "Córdoba",
  Guainía = "Guainía",
  Guaviare = "Guaviare",
  Huila = "Huila",
  LaGuajira = "La Guajira",
  Magdalena = "Magdalena",
  Meta = "Meta",
  Nariño = "Nariño",
  NorteDeSantander = "Norte de Santander",
  Putumayo = "Putumayo",
  Quindío = "Quindío",
  Risaralda = "Risaralda",
  SANAndrésYProvidencia = "San Andrés y Providencia",
  Santander = "Santander",
  Sucre = "Sucre",
  Tolima = "Tolima",
  ValleDelCauca = "Valle del Cauca",
  Vaupés = "Vaupés",
  Vichada = "Vichada",
}

export enum Capital {
  Admin = "admin",
  Empty = "",
  Minor = "minor",
  Primary = "primary",
}

export enum Country {
  Colombia = "Colombia",
}

export enum Iso2 {
  Co = "CO",
}
