export interface User {
  user_id: string;
  image: string;
  email: string;
  rol: string;
  person_id: string;
  password?: string;
  rol_id?: string;
}
