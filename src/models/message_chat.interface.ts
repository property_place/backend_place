export interface MessageChat {
  message: string;
  senderID: string;
  receiverID: string;
  time: string;
  userId: string;
}
