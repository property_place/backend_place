export interface Filters {
  key: string;
  data: DataFilters | DataFiltersDateOrPrice;
}

export interface DataFilters {
  sort?: string;
  type?: string;
}

export interface DataFiltersDateOrPrice {
  key?: string;
  maximum?: string;
  minimum?: string;
  sort?: string;
}
