const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

import multer from "multer";
const storage = require('./middleware/multer_image_uploads');

import { Request, Response, NextFunction } from "express";
import cors from "cors";
import "reflect-metadata";
require("dotenv").config();

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(multer({ storage }).single('image'));
app.use(cookieParser());
app.use("/public", express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// routes
app.use("/api/categories", require("./routes/categories.routes"));
app.use("/api/immovables", require("./routes/immovables.routes"));
app.use("/api/cities", require("./routes/cities.routes"));
app.use("/api/auth", require("./routes/auth.routes"));
app.use("/api/chat", require("./routes/chat.routes"));

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: NextFunction) {
  next(createError(404));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render("error");
});

export default app;
